import 'bootstrap/dist/css/bootstrap.min.css'
import Navbar from '../../Components/Navigation/navbar'
import React, { Component } from 'react';
import { Button, Card, CardBody, CardGroup, Col, Jumbotron, Progress, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';



export default class Register1 extends Component {
    render() {
        let links = {
            color: 'white',
        };

        return (
        <Container>
            <Navbar />
            <Row className={'fluid'}>
        <Col md="auto">
        <div className="text-center">0%</div>
            <Progress />
        </Col>
        </Row>
        <br />
        <Row>
<Col md={'auto'}>
<h3>Redeem Your MIDAS gift card for BTC</h3>
    <p>Scan or type your activation code</p>
</Col>
        </Row>
        <Row>
            <Col>
        <Form>
                <InputGroup className="mb-4">
                    <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                            <i className="fa fa-lock"></i>
                        </InputGroupText>
                    </InputGroupAddon>
                    <Input type="code" placeholder="Activation Code" autoComplete="code" onChange="/login" />
                    </InputGroup>
            </Form>
            </Col>
            <Col><img src='../../static/qr.png' style={{width: 50}} /></Col>
        </Row>
        <Row>
        <Button color ="primary" className="px-4 btn-block" href="/register/register-2">REDEEM</Button>
        </Row>
        </Container>
    );
}
}

