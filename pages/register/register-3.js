import 'bootstrap/dist/css/bootstrap.min.css'
import Navbar from '../../Components/Navigation/navbar'
import React, {Component} from 'react';
import { Alert,
    Button,
    Col,
    Fade,
    Progress,
    Container,
    Form,
    FormGroup,
    Input,
    Row
} from 'reactstrap';

export default class Register3 extends Component {

    constructor(props) {
        super(props);
        this.state = { fadeIn: false };
        this.toggle = this.toggle.bind(this);
    }

    render() {
        let links = {
            color: 'white'
        };

        return (
            <Container>
                <Navbar/>
                <Row fluid>
                    <Col md="auto">
                        <div className="text-center">50%</div>
                        <Progress value={50}/>
                    </Col>
                </Row>
                <br/>
                <Row>
                    <Col md={'auto'} className='text-center'>
                        <h3>Phone Number</h3>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form>
                            <FormGroup>
                                <Input
                                    type="phone"
                                    name="phone"
                                    id="phone"
                                    placeholder="Phone Number"/>
                            </FormGroup>
                        </Form>
                    </Col>
                </Row>
                <Row>
                    <Col>
                    <Button color="primary" className="px-4 btn-block" onClick={this.toggle}>Next</Button>
                    </Col>
                </Row>
                <Fade in={this.state.fadeIn} tag="h5" className="mt-3">
                <br/>
                <br/>
                <br/>
                <Alert>We just sent you a confirmation code! Please enter it below to verify your phone number</Alert>
                <Row>
                    <Col>
                    <Form>
                        <FormGroup>
                            <Input
                                type={'phone'}
                                name={'confirmation'}
                                id={'phoneConf'}
                                placeholder={'Enter Confirmation Code'}/>
                        </FormGroup>
                    </Form>
                    </Col>
                <Col>
                    <Button color="primary" className="px-4 btn-block" href="/register/register-4">Next</Button>
                </Col>
                </Row>
                </Fade>
            </Container>
        );
    }

    toggle() {
        this.setState({
            fadeIn: !this.state.fadeIn
        });
    }

}
