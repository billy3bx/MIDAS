import 'bootstrap/dist/css/bootstrap.min.css'
import Navbar from '../../Components/Navigation/navbar'
import React, { Component } from 'react';
import { Button, Card, CardBody, CardGroup, Col, Jumbotron, Progress, Container, Form, FormGroup, Label, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';



export default class Register4 extends Component {
    render() {
        let links = {
            color: 'white',
        };

        return (
        <Container>
            <Navbar />
            <Row fluid>
        <Col md="auto">
        <div className="text-center">75%</div>
            <Progress value={75}/>
        </Col>
        </Row>
        <br />
        <Row>
<Col md={'auto'} className='text-left'>
<h3>Doberman,</h3></Col>
        </Row>
        <Row className={'border'}>
            <p>The last step to redeeming your MIDAS gift card is choosing where to store your bitcoin.
                New users will most likely be more comfortable redeeming their gift cards to their MIDAS balance. We have taken all the mystery out of cryptocurrency and made it as easy as possible to get started!</p>
        </Row>
        <br />
        <Row>
            <p>Most users will want to use this first option, conveniently storing their funds inside their MIDAS app.</p>
        <Button color ="primary" className="px-4 btn-block" href="/dashboard/dashboard">Finish Registration with MIDAS</Button>
        </Row>
        <Row>
            <p>Advanced users who are familiar with and already own some cryptocurrency can use the external transfer method.</p>
        <Button color ="primary" className="px-4 btn-block" href="/dashboard/transfer">Transfer to External Wallet</Button>
        </Row>
        </Container>
    );
}
}

