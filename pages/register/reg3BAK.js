import 'bootstrap/dist/css/bootstrap.min.css'
import Navbar from '../../Components/Navigation/navbar'
import React, {Component} from 'react';
import {
    Button,
    Col,
    Jumbotron,
    Progress,
    Container,
    Form,
    FormGroup,
    Label,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Row
} from 'reactstrap';

export default class Register3 extends Component {
    render() {
        let links = {
            color: 'white'
        };

        return (
            <Container>
                <Navbar/>
                <Row fluid>
                    <Col md="auto">
                        <div className="text-center">50%</div>
                        <Progress value={50}/>
                    </Col>
                </Row>
                <br/>
                <Row>
                    <Col md={'auto'} className='text-center'>
                        <h3>Doberman,</h3>
                        <h4>To comply with certain SEC regulations we need to gather a few more details
                            before we can take you to your bitcoin deposit.</h4>
                        <h4>Please bear with us, we're almost done!</h4>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form>
                            <Col><FormGroup>
                                <Label for="bday">Date of Birth</Label>
                                <Input type="select" name="birthday" id="name" placeholder="Enter name">
                                    <option>January</option>
                                    <option>February</option>
                                    <option>March</option>
                                    <option>April</option>
                                    <option>May</option>
                                </Input>
                                <Input type="select" name="birthday" id="name" placeholder="Enter name">
                                    <option>01</option>
                                    <option>02</option>
                                    <option>03</option>
                                    <option>04</option>
                                    <option>05</option>
                                </Input>
                                <Input type="select" name="birthday" id="name" placeholder="Enter name">
                                    <option>1988</option>
                                    <option>1989</option>
                                    <option>1990</option>
                                    <option>1991</option>
                                    <option>1992</option>
                                </Input>
                            </FormGroup></Col>
                            <FormGroup>
                                <Label for="country">Country</Label>
                                <Input
                                    type="country"
                                    name="country"
                                    id="country"
                                    placeholder="Country"/>
                            </FormGroup>
                            <FormGroup>
                                <Label for="state">State</Label>
                                <Input
                                    type="state"
                                    name="state"
                                    id="state"
                                    placeholder="State"/>
                            </FormGroup>
                        </Form>
                    </Col>
                </Row>
                <Row>
                    <Button color="primary" className="px-4 btn-block" href="/register/register-4">REGISTER</Button>
                </Row>
            </Container>
        );
    }
}
