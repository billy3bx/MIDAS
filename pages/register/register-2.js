import 'bootstrap/dist/css/bootstrap.min.css'
import Navbar from '../../Components/Navigation/navbar'
import React, { Component } from 'react';
import { Button, Card, CardBody, CardGroup, Col, Jumbotron, Progress, Container, Form, FormGroup, Label, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';



export default class Register2 extends Component {
    render() {
        let links = {
            color: 'white',
        };

        return (
        <Container>
            <Navbar />
            <Row fluid>
        <Col md="auto">
        <div className="text-center">25%</div>
            <Progress value={25}/>
        </Col>
        </Row>
        <br />
        <Row>
<Col md={'auto'} className='text-center'>
<h4>Please enter your information below to create your MIDAS account and get started using our app!</h4></Col>
        </Row>
        <Row>
            <Col>
        <Form>
        <FormGroup>
          <Label for="name">Name</Label>
          <Input type="name" name="name" id="name" placeholder="Enter name" />
        </FormGroup>
        <FormGroup>
          <Label for="email">Email</Label>
          <Input type="email" name="email" id="exampleEmail" placeholder="Enter email address" />
        </FormGroup>
        <FormGroup>
          <Label for="password">Password</Label>
          <Input type="password" name="password" id="password" placeholder="Choose a secure password" />
        </FormGroup>
        </Form>
            </Col>
        </Row>
        <Row>
        <Button color ="primary" className="px-4 btn-block" href="/register/register-3">Next</Button>
        </Row>
        </Container>
    );
}
}

