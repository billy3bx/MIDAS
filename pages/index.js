import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Button, Card, CardBody, CardGroup, Col, Jumbotron, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import Navbar from '../Components/Navigation/navbar'
import Dashboard from "./dashboard/dashboard";

export default class Index extends Component {
    render() {
        
        
        let style = {
            background: 'grey',
            //fontSize: 200
        };
        let links = {
            color: 'white',
        };

        return (

<div>
    <Container>
        <Navbar />
        <Jumbotron fluid className="text-center">
            <h1 className="display-3">Welcome to MIDAS</h1>
            <p className="lead">Redeem your BTC gift card for ca$hhhhh</p>
        </Jumbotron>
        <Row className="justify-content-center">
        <Col md="auto">
            <Button color ="warning" className="px-4 btn-block" href="register/register-1">Redeem Code</Button>
        </Col>
        <Col md="auto">
            <Button color ="primary" className="px-4 btn-block" href="/login">Login</Button>
        </Col>
        </Row>
    </Container>
</div>
        );
    }
}

