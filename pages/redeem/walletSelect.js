import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Alert, Button, Card, CardBody, CardGroup, Col, Jumbotron, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import "react-tabs/style/react-tabs.css";
import HomeMenu from '../../Components/Navigation/homeMenu'
import DashNav from "../../Components/Navigation/dashboardNav";

export default class WalletSelect extends Component {
    render() {
        let links = {
            color: 'white',
        };

        return (

            <div>
                <Container>
                    <DashNav />
                    <h1>Redeem</h1>
                    <p>Choose where to redeem your gift card to.</p>
                    <br />
                    <Row>
                        <Col>
                            <Alert color={'warning'}>New users will most likely be more comfortable redeeming their gift cards to their MIDAS balance. We have taken all the mystery out of cryptocurrency and made it as easy as possible to get started!</Alert>
                            <Button color ="primary" href={'/dashboard/dashboard'} className="px-4 btn-block">Redeem at MIDAS</Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                    <Col>
                        <Alert color={'danger'}>Warning! This is only for advanced users who already have their own cryptocurrency wallet set up with a third party site.</Alert>
                        <Button color ="primary" className="px-4 btn-block" href="/dashboard/transfer">Transfer to External Wallet</Button>
                    </Col>
                </Row>
                <br/>
                    <HomeMenu />
                </Container>
            </div>
        );
    }
}

