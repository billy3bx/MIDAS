import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Button, Card, CardBody, CardGroup, Col, Jumbotron, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import "react-tabs/style/react-tabs.css";
import HomeMenu from '../../Components/Navigation/homeMenu'
import DashNav from "../../Components/Navigation/dashboardNav";

export default class Redeem extends Component {
    render() {
        let links = {
            color: 'white',
        };


        return (

            <div>
                <Container>
                    <DashNav />
                        <h1>Redeem</h1>
                        <p>Type your scan your activation code</p>
                        <br />
                        <Row>
                            <Col>
                            <Form className={'text-align-center'}>
                                <InputGroup className="mb-4">
                                    <Input type="code" placeholder="Activation Code" autoComplete="code" onChange="/login" />
                                </InputGroup>
                            </Form>

                            </Col>
                            <Col>
                                <img src={'../static/qr.png'} style={{width:50, display:'inline'}} />
                            </Col>
                        </Row>
                    <br/>
                    <Row>
                        <Col>
                            <Button color ="primary" className="px-4 btn-block"><a style={links} href="walletSelect">Redeem</a></Button>
                        </Col>
                    </Row>
                <br/>
                <Row>
                    <Col>
                    <a href={'redemption'}>See redemption history</a>
                    </Col>
                </Row>
                    <br/>
                    <HomeMenu />
                </Container>
            </div>
        );
    }
}

