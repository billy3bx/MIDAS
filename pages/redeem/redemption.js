import 'bootstrap/dist/css/bootstrap.min.css'
import React, { Component } from 'react';
import {Alert, Form, FormGroup, Label, Input, Col, Row, Container, Table} from 'reactstrap';
import HomeMenu from "../../Components/Navigation/homeMenu";
import DashNav from "../../Components/Navigation/dashboardNav";

export default class Redemption extends Component {
    render() {
        return(
            <Container>
                <DashNav/>
                    <h1>Redemption History</h1>
                    <Table>
                        <thead>
                        <tr>
                            <th>January</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">1/2/19</th>
                            <td>$25</td>
                            <td>BTC card</td>
                            <td>To MIDAS balance</td>
                        </tr>
                        <tr>
                            <th scope="row">1/3/19</th>
                            <td>$100</td>
                            <td>BTC card</td>
                            <td>To MIDAS balance</td>
                        </tr>
                        <tr>
                            <th scope="row">1/4/19</th>
                            <td>$50</td>
                            <td>BTC card</td>
                            <td>To External Wallet</td>
                        </tr>
                        </tbody>
                    </Table>
            <HomeMenu/>
            </Container>
        );
    }
}

