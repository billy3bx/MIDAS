import 'bootstrap/dist/css/bootstrap.min.css'
import React, { Component } from 'react';
import { Alert, Form, FormGroup, Label, Input, Col, Row, Container } from 'reactstrap';
import DashNav from "../Components/Navigation/dashboardNav";

export default class Profile extends Component {
    render() {
        return(
            <Container>
                <DashNav/>
                <Alert color="warning">Something about profile settings!</Alert>
                <Row className="justify-content-center">
                    <Col md="6">
                        <Form>
                            <FormGroup>
                                <Label for="exampleEmail">Email</Label>
                                <Input type="email" name="email" id="exampleEmail" placeholder="email@email.com"/>
                            </FormGroup>
                            <FormGroup>
                                <Label for="accountnumber">Account Number</Label>
                                <Input type="textarea" name="text" id="exampleText" placeholder="Account Number"/>
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleSelect">Account Type</Label>
                                <Input type="select" name="select" id="exampleSelect">
                                    <option>Checking</option>
                                    <option>Savings</option>
                                </Input>
                            </FormGroup>
                        </Form>
                    </Col>
                </Row>
            </Container>
        );
    }
}

