import 'bootstrap/dist/css/bootstrap.min.css'
import React, { Component } from 'react';
import {Alert, Form, FormGroup, Label, Input, Col, Row, Container, Table} from 'reactstrap';

export default class Transactions extends Component {
    render() {
        return(
            <Container>
                {/*<Row>*/}
                    <h1>Transaction History</h1>
                {/*</Row>*/}
                {/*<Row className={'fluid'}>*/}
                {/*    <Col className={'fluid border'}>*/}
                {/*        <p>BTC -> Coinbase</p>*/}
                {/*        <p>Jan 17th, 2019</p>*/}
                {/*    </Col>*/}
                {/*</Row>*/}
                {/*    <Row className={'fluid border'}>*/}
                {/*        <Col className={'fluid'}>*/}
                {/*        <p>BTC -> Binance</p><p>Jan 27th, 2019</p>*/}
                {/*        </Col>*/}
                {/*    </Row>*/}
                    <Table>
                        <thead>
                        <tr>
                            <th>January</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">1/2/19</th>
                            <td>BTC</td>
                            <td>0.013</td>
                            <td>To Binance</td>
                            <td>TX ID</td>
                        </tr>
                        <tr>
                            <th scope="row">1/3/19</th>
                            <td>BTC</td>
                            <td>1</td>
                            <td>To Coinbase</td>
                            <td>TX ID</td>
                        </tr>
                        <tr>
                            <th scope="row">1/4/19</th>
                            <td>BTC</td>
                            <td>0.001</td>
                            <td>To External Address</td>
                            <td>TX ID</td>
                        </tr>
                        </tbody>
                    </Table>
            </Container>
        );
    }
}

