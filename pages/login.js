import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Button, Card, CardBody, CardGroup, Col, Jumbotron, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import "react-tabs/style/react-tabs.css";
import Navbar from '../Components/Navigation/navbar'

export default class Login extends Component {
    render() {

        let links = {
            color: 'white',
        };

        return (

            <div>
                <Navbar />
                <Container fluid>
                    <Jumbotron fluid className="text-center">
                        <h1 className="display-3">Welcome to MIDAS</h1>
                        {/*<p className="lead">Scan your QR code below to get started.</p>*/}
                    </Jumbotron>
                    <Row>
                        <Col lg="4">
                        </Col>
                        <Col lg="auto">

                            <Form>
                                <p className="text-muted">Sign in to access your account</p>
                                <InputGroup className="mb-3">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <i className="fa fa-user"></i>
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="text" placeholder="Email Address" autoComplete="email" onChange={this.updateEmail}/>
                                </InputGroup>
                                <InputGroup className="mb-4">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <i className="fa fa-lock"></i>
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="password" placeholder="Password" autoComplete="current-password" onChange={this.updatePassword}/>
                                </InputGroup>
                                <Row className={"justify-content-center"}>
                                    <Col>
                                        {/*<Button color ="primary" onClick={ () => this.login()} className="px-4 btn-block"><a style={links} href="/dashboard">Login</a></Button>*/}
                                        <Button color ="primary" className="px-4 btn-block" href="/dashboard/dashboard">Login</Button>
                                    </Col>
                                </Row>
                            </Form>

                            {/*<Tabs>*/}
                            {/*    <TabList>*/}
                            {/*        <Tab>Login</Tab>*/}
                            {/*        <Tab>Sign up</Tab>*/}
                            {/*    </TabList>*/}

                            {/*    <TabPanel>*/}
                            {/*        <Form>*/}
                            {/*            <p className="text-muted">Sign in to access your account</p>*/}
                            {/*            <InputGroup className="mb-3">*/}
                            {/*                <InputGroupAddon addonType="prepend">*/}
                            {/*                    <InputGroupText>*/}
                            {/*                        <i className="fa fa-user"></i>*/}
                            {/*                    </InputGroupText>*/}
                            {/*                </InputGroupAddon>*/}
                            {/*                <Input type="text" placeholder="Email Address" autoComplete="email" onChange={this.updateEmail}/>*/}
                            {/*            </InputGroup>*/}
                            {/*            <InputGroup className="mb-4">*/}
                            {/*                <InputGroupAddon addonType="prepend">*/}
                            {/*                    <InputGroupText>*/}
                            {/*                        <i className="fa fa-lock"></i>*/}
                            {/*                    </InputGroupText>*/}
                            {/*                </InputGroupAddon>*/}
                            {/*                <Input type="password" placeholder="Password" autoComplete="current-password" onChange={this.updatePassword}/>*/}
                            {/*            </InputGroup>*/}
                            {/*            <Row className={"justify-content-center"}>*/}
                            {/*                <Col>*/}
                            {/*                    /!*<Button color ="primary" onClick={ () => this.login()} className="px-4 btn-block"><a style={links} href="/dashboard">Login</a></Button>*!/*/}
                            {/*                    <Button color ="primary" className="px-4 btn-block" href="/dashboard/dashboard">Login</Button>*/}
                            {/*                </Col>*/}
                            {/*            </Row>*/}
                            {/*        </Form>*/}
                            {/*    </TabPanel>*/}
                            {/*    <TabPanel>*/}
                            {/*        <Form>*/}
                            {/*            <p className="text-muted">Create an account to get started</p>*/}
                            {/*            <InputGroup className="mb-3">*/}
                            {/*                <InputGroupAddon addonType="prepend">*/}
                            {/*                    <InputGroupText>*/}
                            {/*                        <i className="fa fa-user"></i>*/}
                            {/*                    </InputGroupText>*/}
                            {/*                </InputGroupAddon>*/}
                            {/*                <Input type="text" placeholder="Full Name" autoComplete="name" onChange={this.updateName}/>*/}
                            {/*            </InputGroup>*/}
                            {/*            <InputGroup className="mb-3">*/}
                            {/*                <InputGroupAddon addonType="prepend">*/}
                            {/*                    <InputGroupText>*/}
                            {/*                        <i className="fa fa-user"></i>*/}
                            {/*                    </InputGroupText>*/}
                            {/*                </InputGroupAddon>*/}
                            {/*                <Input type="string" placeholder="Phone Number" autoComplete="phonenumber" onChange={this.updatePhone}/>*/}
                            {/*            </InputGroup>*/}
                            {/*            <InputGroup className="mb-3">*/}
                            {/*                <InputGroupAddon addonType="prepend">*/}
                            {/*                    <InputGroupText>*/}
                            {/*                        <i className="fa fa-user"></i>*/}
                            {/*                    </InputGroupText>*/}
                            {/*                </InputGroupAddon>*/}
                            {/*                <Input type="text" placeholder="State" autoComplete="state" onChange={this.updateAddress}/>*/}
                            {/*            </InputGroup>*/}
                            {/*            <InputGroup className="mb-3">*/}
                            {/*                <InputGroupAddon addonType="prepend">*/}
                            {/*                    <InputGroupText>*/}
                            {/*                        <i className="fa fa-user"></i>*/}
                            {/*                    </InputGroupText>*/}
                            {/*                </InputGroupAddon>*/}
                            {/*                <Input type="text" placeholder="Email Address" autoComplete="email" onChange={this.updateEmail}/>*/}
                            {/*            </InputGroup>*/}
                            {/*            <InputGroup className="mb-4">*/}
                            {/*                <InputGroupAddon addonType="prepend">*/}
                            {/*                    <InputGroupText>*/}
                            {/*                        <i className="fa fa-lock"></i>*/}
                            {/*                    </InputGroupText>*/}
                            {/*                </InputGroupAddon>*/}
                            {/*                <Input type="password" placeholder="Password" autoComplete="current-password" onChange={this.updatePassword}/>*/}
                            {/*            </InputGroup>*/}
                            {/*            <Row className={"justify-content-center"}>*/}
                            {/*                <Col>*/}
                            {/*                    <Button color ="primary" onClick={ () => this.login()} className="px-4 btn-block">Sign Up</Button>*/}
                            {/*                </Col>*/}
                            {/*            </Row>*/}
                            {/*        </Form>*/}
                            {/*    </TabPanel>*/}
                            {/*</Tabs>*/}
                        </Col>
                        <Col lg="4">
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

