import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Alert, Button, Card, CardBody, CardGroup, Col, Jumbotron, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import "react-tabs/style/react-tabs.css";
import HomeMenu from '../../Components/Navigation/homeMenu'
import DashNav from "../../Components/Navigation/dashboardNav";

export default class Transfer extends Component {
    render() {
        let links = {
            color: 'white',
        };


        return (

            <div>
                <Container>
                    <DashNav />
                    <h1>External Wallet Transfer</h1>
                    <Alert color={'danger'}>Warning! This will transfer funds from your balance to an external address.</Alert>
                    <br />
                    <Row>
                        <Col>
                            <Form className={'text-align-center'}>
                                <InputGroup className="mb-4">
                                    <Input type="code" placeholder="Destination Address" autoComplete="code" onChange="/login" />
                                </InputGroup>
                            </Form>

                        </Col>
                        <Col>
                            <img src={'../static/qr.png'} style={{width:50, display:'inline'}} />
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col>
                            <Button color ="primary" className="px-4 btn-block" href="/dashboard/dashboard">Submit</Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col>
                            <Alert color={'danger'}>MIDAS is not responsible for lost funds due to external transfers. Please double check your destination address.</Alert>
                        </Col>
                    </Row>
                    <br/>
                    <HomeMenu />
                </Container>
            </div>
        );
    }
}

