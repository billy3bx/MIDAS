import 'bootstrap/dist/css/bootstrap.min.css'
import React, { Component } from 'react';
import { Alert, Form, FormGroup, Label, Input, Col, Row, Container } from 'reactstrap';
import DashNav from "../../Components/Navigation/dashboardNav";
import HomeMenu from "../../Components/Navigation/homeMenu";

export default class Rewards extends Component {
    render() {
        return(
            <Container>
                <DashNav/>
                <br/>
                <Row className="text-center">
                    <Col md="6">
                        <h2>Affiliate links and stuff</h2>
                        <img src={'https://www.affiliatemarketertraining.com/wp-content/uploads/2016/07/how-to-incorporate-affiliate-links-on-your-blog-without-feeling-scammy.jpg'} style={{width:'100%'}}/>
                    </Col>
                </Row>
                <br/>
                <HomeMenu/>
            </Container>
        );
    }
}

