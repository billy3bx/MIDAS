import 'bootstrap/dist/css/bootstrap.min.css'
import React, { Component } from 'react';
import { Alert, Form, FormGroup, Label, Input, Col, Row, Container } from 'reactstrap';
import DashNav from "../../Components/Navigation/dashboardNav";
import HomeMenu from "../../Components/Navigation/homeMenu";

export default class Rewards extends Component {
    render() {
        return(
            <Container>
                <DashNav/>
                <br/>
                <Row className="text-center">
                    <Col md="6">
                        <p>Get your rewards up in this piece!</p>
                        <img src={'https://www.suntrust.com/content/dam/suntrust/us/en/shared/2017/icon-library/glyph-rewards-program-cc.png'} />
                    </Col>
                </Row>
                <br/>
                <HomeMenu/>
            </Container>
        );
    }
}

