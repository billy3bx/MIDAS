import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Alert, Button, Card, CardBody, CardGroup, Col, Jumbotron, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Table } from 'reactstrap';
import "react-tabs/style/react-tabs.css";
import HomeMenu from '../../Components/Navigation/homeMenu'
import DashNav from "../../Components/Navigation/dashboardNav";

export default class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false
        }
    }

    render() {


        let style = {
            background: 'grey',
            //fontSize: 200
        };
        let links = {
            color: 'white',
        };

        return (

            <div>
                <Container>
                    <DashNav />
                    <Jumbotron fluid className="text-center">
                        <Alert color="success" onLoad={()=>{this.onShowAlert()}}>Success! You've redeemed 0.013 BTC!</Alert>
                        <h1>Welcome Doberman</h1>
                        <h3>Your balances are:</h3>
                        <br />
                        <Row className={'fluid'}>
                            <Col>
                            <Table bordered>
                                <thead>
                                <tr>
                                    <th>Cryptocurrency</th>
                                    <th>USD Value</th>
                                    <th>Balance</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">BTC</th>
                                    <td>$50.00</td>
                                    <td>0.013</td>
                                </tr>
                                <tr>
                                    <th scope="row">ETH</th>
                                    <td>$131.00</td>
                                    <td>1.000</td>
                                </tr>
                                <tr>
                                    <th scope="row">XMR</th>
                                    <td>$206.60</td>
                                    <td>4.000</td>
                                </tr>
                                </tbody>
                            </Table>
                            </Col>
                        </Row>
                    </Jumbotron>
                    <HomeMenu />
                </Container>
            </div>
        );
    }
}

