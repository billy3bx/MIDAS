import React from 'react';
import { Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';

export default class Header extends React.Component {

    render() {
        let navStyle ={
            color: 'white'
        }
        return (
            <div>
                <Navbar color="dark" dark expand="md">
                    <NavbarBrand href="/">MIDAS</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                        <Nav className="ml-auto" navbar>
                            {/* <NavItem>
                                <NavLink href="/settings" style={navStyle}>Settings</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/about" style={navStyle}>About</NavLink>
                            </NavItem> */}
                        </Nav>
                </Navbar>
            </div>
        );
    }
}