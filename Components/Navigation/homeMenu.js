import React from 'react';
import { Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
var FontAwesome = require('react-fontawesome');


export default class HomeMenu extends React.Component {
            render() {
        let navStyle ={
            color: 'white',
            width: 70,
            marginLeft: '25%'
        }

        let centerNav = {
            float: 'none',
            textAlign: 'center',
            verticalAlign: 'top',
            justifyContent: 'center',
            display: 'inline-block'
            }

                return (
            <div className={'justify-content-center'}>
                <Navbar className={centerNav} color="dark" dark expand="md">
                    {/*<NavbarBrand href="/">MIDAS</NavbarBrand>*/}
                    <NavbarBrand href="/dashboard/dashboard" className="mr-auto"><img style ={navStyle} src ={'https://image.flaticon.com/icons/png/512/69/69524.png'}/></NavbarBrand>
                    <NavbarBrand href="/redeem/redeem" className="mr-auto"><img style ={navStyle} src ={'https://cdn2.iconfinder.com/data/icons/social-productivity-line-art-2/128/credit-card-512.png'}/></NavbarBrand>
                    <NavbarBrand href="/dashboard/transfer" className="mr-auto"><img style ={navStyle} src ={'https://cdn0.iconfinder.com/data/icons/mintab-outline-for-ios-2/30/transfer-shuffle-arrow-back-forward-512.png'}/></NavbarBrand>
                    <NavbarBrand href="/dashboard/shop" className="mr-auto"><img style ={navStyle} src ={'https://image.flaticon.com/icons/png/512/2/2772.png'}/></NavbarBrand>

                        {/*<Nav className="ml-auto" navbar>*/}
                        {/*    <NavItem>*/}
                        {/*        <NavLink href="/dashboard" style={navStyle}><img style={navStyle} src={'https://image.flaticon.com/icons/png/512/69/69524.png'} /></NavLink>*/}
                        {/*    </NavItem>*/}
                        {/*    <NavItem>*/}
                        {/*        <NavLink href="/redeem" style={navStyle}><img style={navStyle} src={'https://cdn2.iconfinder.com/data/icons/social-productivity-line-art-2/128/credit-card-512.png'} /></NavLink>*/}
                        {/*    </NavItem>*/}
                        {/*    <NavItem>*/}
                        {/*        <NavLink href="/transfer" style={navStyle}><img style={navStyle} src={'https://cdn0.iconfinder.com/data/icons/mintab-outline-for-ios-2/30/transfer-shuffle-arrow-back-forward-512.png'} /></NavLink>*/}
                        {/*    </NavItem>*/}
                        {/*    <NavItem>*/}
                        {/*        <NavLink href="/shop" style={navStyle}><img style={navStyle} src={'https://image.flaticon.com/icons/png/512/2/2772.png'} /></NavLink>*/}
                        {/*    </NavItem>*/}
                        {/*</Nav>*/}
                </Navbar>
            </div>
        );
    }
}