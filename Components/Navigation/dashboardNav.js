import React from 'react';
import { Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';

export default class DashNav extends React.Component {

    render() {
        let navStyle ={
            color: 'white'
        }
        let imgStyle ={
            width: 60,
            paddingLeft: 25,
            marginLeft: 65
        }
        return (
            <div>
                <Navbar color="dark" dark expand="md">
                        <Nav navbar-horizontal>
                            <NavbarBrand href="/dashboard/settings"><img style={imgStyle} src={'http://simpleicon.com/wp-content/uploads/user1.png'} /></NavbarBrand>
                            <NavbarBrand href="/dashboard/dashboard"><img style={imgStyle} src={'https://www.midasbcn.com/wp-content/uploads/2017/09/Midas-Logo-Shadow.png'} /></NavbarBrand>
                            <NavbarBrand href="/dashboard/rewards"><img style={imgStyle} src={'https://cdn2.iconfinder.com/data/icons/holidays-2-1/128/Christmas-Present-Gifts-Award-Birthday-512.png'} /></NavbarBrand>
                        </Nav>
                </Navbar>
            </div>
        );
    }
}